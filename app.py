#!/usr/bin/env python
# encoding: utf-8
from flask import Flask, request, jsonify
from project.XMLParser import XMLParser

app = Flask(__name__)
@app.route('/')
def index():
    return "<html><title>Welcome to Abbo Search API!</title><body><h1>Welcome to Abbo Search API!</h1></body></html>"

@app.route('/findAbboByUser', methods=['GET'])
def query_records():
    userId = request.args.get('userId')

    xml_file_name = 'subscribers_modified.xml'
    parser = XMLParser(xml_file_name)
    abbo_json = parser.getAbboDistributionString(userId)

    return jsonify(abbo_json)

app.run(host='0.0.0.0',debug=True,port='5000')
#app.run()

