import requests
import unittest
import json

class TestXMLParser(unittest.TestCase):

    def countAbboByType(self, abbo_json, abbo_type):
        count = 0
        for i in range(0, len(abbo_json)):
            if (abbo_json[i]['abbo_type'] == abbo_type):
                count += 1
        return count

    def test_get_abbos_for_userid_625998737(self):
        userId = "625998737"
        response = requests.get("http://127.0.0.1:5000/findAbboByUser?userId=" + userId)
        print(response.status_code)
        assert response.status_code == 200
        abbo = json.loads(response.json())
        # print(abbo)

        self.assertEqual(0, self.countAbboByType(abbo, "abbo-365"))
        self.assertEqual(24, self.countAbboByType(abbo, "abbo-30"))
        self.assertEqual(4, self.countAbboByType(abbo, "abbo-7"))
        self.assertEqual(9, self.countAbboByType(abbo, "abbo-1"))

    def test_get_abbos_for_userid_520392515(self):
        userId = "520392515"
        response = requests.get("http://127.0.0.1:5000/findAbboByUser?userId=" + userId)
        print(response.status_code)
        assert response.status_code == 200
        abbo = json.loads(response.json())
        # print(abbo)

        self.assertEqual(0, self.countAbboByType(abbo, "abbo-365"))
        self.assertEqual(3, self.countAbboByType(abbo, "abbo-30"))
        self.assertEqual(1, self.countAbboByType(abbo, "abbo-7"))
        self.assertEqual(2, self.countAbboByType(abbo, "abbo-1"))

    def test_get_abbos_for_userid_629723578(self):
        userId = "629723578"
        response = requests.get("http://127.0.0.1:5000/findAbboByUser?userId=" + userId)
        print(response.status_code)
        assert response.status_code == 200
        abbo = json.loads(response.json())
        # print(abbo)

        self.assertEqual(0, self.countAbboByType(abbo, "abbo-365"))
        self.assertEqual(3, self.countAbboByType(abbo, "abbo-30"))
        self.assertEqual(1, self.countAbboByType(abbo, "abbo-7"))
        self.assertEqual(2, self.countAbboByType(abbo, "abbo-1"))

    def test_get_abbos_for_userid_11111111(self):
        userId = "11111111"
        response = requests.get("http://127.0.0.1:5000/findAbboByUser?userId=" + userId)
        print(response.status_code)
        assert response.status_code == 200
        abbo = json.loads(response.json())
        # print(abbo)

        self.assertEqual(0, self.countAbboByType(abbo,"abbo-365"))
        self.assertEqual(3, self.countAbboByType(abbo,"abbo-30"))
        self.assertEqual(1, self.countAbboByType(abbo,"abbo-7"))
        self.assertEqual(3, self.countAbboByType(abbo,"abbo-1"))

    def test_get_abbos_for_userid_22222222(self):
        userId = "22222222"
        response = requests.get("http://127.0.0.1:5000/findAbboByUser?userId=" + userId)
        print(response.status_code)
        assert response.status_code == 200
        abbo = json.loads(response.json())
        # print(abbo)

        self.assertEqual(0, self.countAbboByType(abbo,"abbo-365"))
        self.assertEqual(3, self.countAbboByType(abbo,"abbo-30"))
        self.assertEqual(1, self.countAbboByType(abbo,"abbo-7"))
        self.assertEqual(4, self.countAbboByType(abbo,"abbo-1"))

    def test_get_abbos_for_userid_33333333(self):
        userId = "33333333"
        response = requests.get("http://127.0.0.1:5000/findAbboByUser?userId=" + userId)
        print(response.status_code)
        assert response.status_code == 200
        abbo = json.loads(response.json())
        # print(abbo)

        self.assertEqual(1, self.countAbboByType(abbo,"abbo-365"))
        self.assertEqual(3, self.countAbboByType(abbo,"abbo-30"))
        self.assertEqual(1, self.countAbboByType(abbo,"abbo-7"))
        self.assertEqual(4, self.countAbboByType(abbo,"abbo-1"))
