import unittest
import json
from project.XMLParser import XMLParser

class TestXMLParser(unittest.TestCase):

    def countAbboByType(self, abbo_json, abbo_type):
        count = 0
        for i in range(0, len(abbo_json)):
            if (abbo_json[i]['abbo_type'] == abbo_type):
                count += 1
        return count

    def test_xml_parser_for_given_user(self):
        """
            Test that XMLParser return the right JSON file for userId 629723578
        """
        xml_file_name = '../subscribers_modified.xml'
        userId = "629723578"
        parser = XMLParser(xml_file_name)
        # data = [1, 2, 3]
        # result = sum(data)
        json_string = parser.getAbboDistributionString(userId)
        parser.showXMLforUser(userId)
        # print(json_string)
        abbo_list = json.loads(json_string)
        print(json.dumps(abbo_list, indent=4, sort_keys=True))
        # print(json.dumps(abbo_list[1], indent=4, sort_keys=True))
        self.assertEqual(len(abbo_list), 6)
        abbo_30_list = [obj for obj in abbo_list if obj['abbo_type'] == "abbo-30"]
        self.assertEqual(len(abbo_30_list),3)
        abbo_7_list = [obj for obj in abbo_list if obj['abbo_type'] == "abbo-7"]
        self.assertEqual(len(abbo_7_list),1)
        abbo_1_list = [obj for obj in abbo_list if obj['abbo_type'] == "abbo-1"]
        self.assertEqual(len(abbo_1_list),2)

    @unittest.skip
    def test_xml_parser_for_user_with_duplications(self):
        """
            Test that XMLParser return the right JSON file for userId 520392515 with 2 duplications
        """
        xml_file_name = '../subscribers_modified.xml'
        userId = "520392515"
        parser = XMLParser(xml_file_name)
        json_string = parser.getAbboDistributionString(userId)
        # print(json_string)
        abbo_list = json.loads(json_string)
        # print(json.dumps(abbo_list, indent=4, sort_keys=True))

    @unittest.skip
    def test_xml_parser_for_duplications(self):
        """
            Test that XMLParser return the right da file for userId 520392515 with 2 duplications
        """
        xml_file_name = '../subscribers_modified.xml'

    @unittest.skip
    def test_xml_parser_show_xml_users(self):
        """
            Test that showXMLforUser method
        """
        xml_file_name = '../subscribers_modified.xml'
        userId = "520392515"
        parser = XMLParser(xml_file_name)
        json_string = parser.showXMLforUser(userId)

    @unittest.skip
    def test_xml_parser_show_dates_by_user(self):
        """
            Test that showDatesByUser method
        """
        xml_file_name = '../subscribers_modified.xml'
        userId = "520392515"
        parser = XMLParser(xml_file_name)
        json_string = parser.showDatesByUser(userId)

    @unittest.skip
    def test_xml_parser_get_diff_date_time_by_user(self):
        """
            Test that getDiffDateTime method
        """
        xml_file_name = '../subscribers_modified.xml'
        userId = "520392515"
        parser = XMLParser(xml_file_name)
        json_string = parser.getDiffDateTimeByUser(userId)
        # print(json_string)

    def test_xml_parser_get_diff_date_time_from_list(self):
        xml_file_name = '../subscribers_modified.xml'
        userId = "629723578"
        parser = XMLParser(xml_file_name)
        diff_list = parser.getDiffDateTimeListByUser(userId)
        # print([str(item) for item in diff_list])
        self.assertEqual(1, len(diff_list))
        self.assertEqual(99,parser.getDaysByUser(userId))

    def test_xml_parser_get_diff_date_time_from_list_with_2_duplications(self):
        xml_file_name = '../subscribers_modified.xml'
        userId = "520392515"
        parser = XMLParser(xml_file_name)
        diff_list = parser.getDiffDateTimeListByUser(userId)
        # print([str(item) for item in diff_list])
        self.assertEqual(2,len(diff_list))

    def test_xml_parser_get_days_when_there_are_2_duplications(self):
        xml_file_name = '../subscribers_modified.xml'
        userId = "520392515"
        parser = XMLParser(xml_file_name)
        diff_list = parser.getDiffDateTimeListByUser(userId)
        # print([str(item) for item in diff_list])
        self.assertEqual(97,parser.getDaysListByUser(userId)[0])
        self.assertEqual(2, parser.getDaysListByUser(userId)[1])

    def test_xml_parser_get_weeks_when_there_are_2_duplications(self):
        xml_file_name = '../subscribers_modified.xml'
        userId = "520392515"
        parser = XMLParser(xml_file_name)
        diff_list = parser.getDiffDateTimeListByUser(userId)
        # print([str(item) for item in diff_list])
        # print(parser.getDaysListByUser(userId))
        # print(97//7)
        self.assertEqual(13,parser.getWeeksListByUser(userId)[0])
        self.assertEqual(0, parser.getWeeksListByUser(userId)[1])

    def test_xml_parser_get_months_when_there_are_2_duplications(self):
        xml_file_name = '../subscribers_modified.xml'
        userId = "520392515"
        parser = XMLParser(xml_file_name)
        diff_list = parser.getDiffDateTimeListByUser(userId)
        # print([str(item) for item in diff_list])
        # print(parser.getDaysListByUser(userId))
        # print(97//7)
        self.assertEqual(3,parser.getMonthsListByUser(userId)[0])
        self.assertEqual(0, parser.getMonthsListByUser(userId)[1])

    def test_xml_parser_get_years_when_there_are_2_duplications(self):
        xml_file_name = '../subscribers_modified.xml'
        userId = "520392515"
        parser = XMLParser(xml_file_name)
        diff_list = parser.getDiffDateTimeListByUser(userId)
        # print([str(item) for item in diff_list])
        # print(parser.getDaysListByUser(userId))
        # print(97//7)
        self.assertEqual(0,parser.getYearsListByUser(userId)[0])
        self.assertEqual(0, parser.getYearsListByUser(userId)[1])

    def test_xml_parser_get_time_distribution_when_there_are_2_duplications(self):
        xml_file_name = '../subscribers_modified.xml'
        userId = "520392515"
        parser = XMLParser(xml_file_name)
        diff_list = parser.getDiffDateTimeListByUser(userId)
        # print([str(item) for item in diff_list])
        # print(parser.getDaysListByUser(userId))
        # print(97//7)
        # self.assertEqual(0,parser.getYearsListByUser(userId)[0])
        # self.assertEqual(0, parser.getYearsListByUser(userId)[1])

        parser.showXMLforUser(userId)

        years = int(parser.getYearsListByUser(userId)[0])
        months = int(parser.getMonthsListByUser(userId)[0])
        weeks = int(parser.getWeeksListByUser(userId)[0])
        days = int(parser.getDaysListByUser(userId)[0])

        self.assertEqual(0,years)
        self.assertEqual(3,months)
        self.assertEqual(13,weeks)
        self.assertEqual(97,days)

        time_distribution_1 = parser.getTimeDistributionFromDates(years, months, weeks, days)
        print(time_distribution_1)

        self.assertEqual(0,time_distribution_1['time_distribution']['years'])
        self.assertEqual(3,time_distribution_1['time_distribution']['months'])
        self.assertEqual(13,time_distribution_1['time_distribution']['weeks'])
        self.assertEqual(97,time_distribution_1['time_distribution']['days'])

        self.assertEqual(0,time_distribution_1['remaining_time_distribution']['years'])
        self.assertEqual(3,time_distribution_1['remaining_time_distribution']['months'])
        self.assertEqual(1,time_distribution_1['remaining_time_distribution']['weeks'])
        self.assertEqual(0,time_distribution_1['remaining_time_distribution']['days'])

        years = int(parser.getYearsListByUser(userId)[1])
        months = int(parser.getMonthsListByUser(userId)[1])
        weeks = int(parser.getWeeksListByUser(userId)[1])
        days = int(parser.getDaysListByUser(userId)[1])

        self.assertEqual(0,years)
        self.assertEqual(0,months)
        self.assertEqual(0,weeks)
        self.assertEqual(2,days)

        time_distribution_2 = parser.getTimeDistributionFromDates(years, months, weeks, days)
        print(time_distribution_2)

        self.assertEqual(0,time_distribution_2['time_distribution']['years'])
        self.assertEqual(0,time_distribution_2['time_distribution']['months'])
        self.assertEqual(0,time_distribution_2['time_distribution']['weeks'])
        self.assertEqual(2,time_distribution_2['time_distribution']['days'])

        self.assertEqual(0,time_distribution_2['remaining_time_distribution']['years'])
        self.assertEqual(0,time_distribution_2['remaining_time_distribution']['months'])
        self.assertEqual(0,time_distribution_2['remaining_time_distribution']['weeks'])
        self.assertEqual(2,time_distribution_2['remaining_time_distribution']['days'])

    def test_xml_parser_get_abbo_distribution_when_there_are_2_duplications(self):
        xml_file_name = '../subscribers_modified.xml'
        userId = "520392515"
        parser = XMLParser(xml_file_name)
        parser.showXMLforUser(userId)
        # time_distribution_list = parser.getTimeDistributionListByUser(userId)
        # start_date_list = parser.getStartDatesListByUser(userId)
        # time_distribution_dict = {}
        # for i in range(0,len(start_date_list)):
        #     time_distribution_dict['start_date_' + str(i)] = start_date_list[i]
        #     time_distribution_dict['time_distribution_' + str(i)] = time_distribution_list
        # print(time_distribution_dict)
        # print(time_distribution_dict.values())
        # abbo_list = parser.getAbboDistribution(userId, start_date_list[0], time_distribution_list[0])
        abbo_json_string = parser.getAbboDistributionString(userId)
        abbo_json = json.loads(abbo_json_string)
        print(json.dumps(abbo_json, indent=4, sort_keys=True))

        # count = 0
        # for i in range(0,len(abbo_json)):
        #     if(abbo_json[i]['abbo_type'] == 'abbo-30'):
        #         count += 1
        self.assertEqual(0, self.countAbboByType(abbo_json,"abbo-365"))
        self.assertEqual(3, self.countAbboByType(abbo_json,"abbo-30"))
        self.assertEqual(1, self.countAbboByType(abbo_json,"abbo-7"))
        self.assertEqual(2, self.countAbboByType(abbo_json,"abbo-1"))

    def test_xml_parser_get_abbo_distribution(self):
        xml_file_name = '../subscribers_modified.xml'
        userId = "629723578"
        parser = XMLParser(xml_file_name)
        parser.showXMLforUser(userId)
        abbo_json_string = parser.getAbboDistributionString(userId)
        abbo_json = json.loads(abbo_json_string)

        print(json.dumps(abbo_json, indent=4, sort_keys=True))

        self.assertEqual(0, self.countAbboByType(abbo_json,"abbo-365"))
        self.assertEqual(3, self.countAbboByType(abbo_json,"abbo-30"))
        self.assertEqual(1, self.countAbboByType(abbo_json,"abbo-7"))
        self.assertEqual(2, self.countAbboByType(abbo_json,"abbo-1"))

    def test_xml_parser_get_abbo_distribution_on_fake_user_2_days(self):
        """
            Test that XMLParser for multiple days
        """
        xml_file_name = '../subscribers_modified.xml'
        userId = "11111111"
        parser = XMLParser(xml_file_name)
        parser.showXMLforUser(userId)
        abbo_json_string = parser.getAbboDistributionString(userId)
        abbo_json = json.loads(abbo_json_string)

        print(json.dumps(abbo_json, indent=4, sort_keys=True))

        self.assertEqual(0, self.countAbboByType(abbo_json,"abbo-365"))
        self.assertEqual(3, self.countAbboByType(abbo_json,"abbo-30"))
        self.assertEqual(1, self.countAbboByType(abbo_json,"abbo-7"))
        self.assertEqual(3, self.countAbboByType(abbo_json,"abbo-1"))

    def test_xml_parser_get_abbo_distribution_on_fake_user_3_days(self):
        """
            Test that XMLParser for multiple days (2)
        """
        xml_file_name = '../subscribers_modified.xml'
        userId = "22222222"
        parser = XMLParser(xml_file_name)
        parser.showXMLforUser(userId)
        abbo_json_string = parser.getAbboDistributionString(userId)
        abbo_json = json.loads(abbo_json_string)

        print(json.dumps(abbo_json, indent=4, sort_keys=True))

        self.assertEqual(0, self.countAbboByType(abbo_json,"abbo-365"))
        self.assertEqual(3, self.countAbboByType(abbo_json,"abbo-30"))
        self.assertEqual(1, self.countAbboByType(abbo_json,"abbo-7"))
        self.assertEqual(4, self.countAbboByType(abbo_json,"abbo-1"))

    def test_xml_parser_get_abbo_distribution_on_fake_user_1_year(self):
        """
            Test that XMLParser where there is 1 year
        """
        xml_file_name = '../subscribers_modified.xml'
        userId = "33333333"
        parser = XMLParser(xml_file_name)
        parser.showXMLforUser(userId)
        abbo_json_string = parser.getAbboDistributionString(userId)
        abbo_json = json.loads(abbo_json_string)

        print(json.dumps(abbo_json, indent=4, sort_keys=True))

        self.assertEqual(1, self.countAbboByType(abbo_json,"abbo-365"))
        self.assertEqual(3, self.countAbboByType(abbo_json,"abbo-30"))
        self.assertEqual(1, self.countAbboByType(abbo_json,"abbo-7"))
        self.assertEqual(4, self.countAbboByType(abbo_json,"abbo-1"))


    def test_xml_parser_get_abbo_distribution_when_there_are_3_duplications(self):
        xml_file_name = '../subscribers_modified.xml'
        userId = "625998737"
        parser = XMLParser(xml_file_name)
        parser.showXMLforUser(userId)
        abbo_json_string = parser.getAbboDistributionString(userId)
        abbo_json = json.loads(abbo_json_string)
        print(json.dumps(abbo_json, indent=4, sort_keys=True))

        self.assertEqual(0, self.countAbboByType(abbo_json,"abbo-365"))
        self.assertEqual(24, self.countAbboByType(abbo_json,"abbo-30"))
        self.assertEqual(4, self.countAbboByType(abbo_json,"abbo-7"))
        self.assertEqual(9, self.countAbboByType(abbo_json,"abbo-1"))


    def test_xml_parser_get_time_distribution_list_by_user_when_there_are_2_duplications(self):
        xml_file_name = '../subscribers_modified.xml'
        userId = "520392515"
        parser = XMLParser(xml_file_name)
        # abbo_list = parser.getAbboDistribution2(userId)
        # print(parser.getTimeDistributionListByUser(userId))
        time_distribution_list = parser.getTimeDistributionListByUser(userId)
        self.assertEqual(2,len(time_distribution_list))

        self.assertEqual(0, time_distribution_list[0]['time_distribution']['years']) # years
        self.assertEqual(3, time_distribution_list[0]['time_distribution']['months'])
        self.assertEqual(13, time_distribution_list[0]['time_distribution']['weeks'])
        self.assertEqual(97, time_distribution_list[0]['time_distribution']['days'])

        self.assertEqual(0, time_distribution_list[0]['remaining_time_distribution']['years'])
        self.assertEqual(3, time_distribution_list[0]['remaining_time_distribution']['months'])
        self.assertEqual(1, time_distribution_list[0]['remaining_time_distribution']['weeks'])
        self.assertEqual(0, time_distribution_list[0]['remaining_time_distribution']['days'])

        self.assertEqual(0, time_distribution_list[1]['time_distribution']['years']) # years
        self.assertEqual(0, time_distribution_list[1]['time_distribution']['months'])
        self.assertEqual(0, time_distribution_list[1]['time_distribution']['weeks'])
        self.assertEqual(2, time_distribution_list[1]['time_distribution']['days'])

        self.assertEqual(0, time_distribution_list[1]['remaining_time_distribution']['years'])
        self.assertEqual(0, time_distribution_list[1]['remaining_time_distribution']['months'])
        self.assertEqual(0, time_distribution_list[1]['remaining_time_distribution']['weeks'])
        self.assertEqual(2, time_distribution_list[1]['remaining_time_distribution']['days'])

    def test_xml_parser_get_start_date_list_when_there_are_2_duplications(self):
        xml_file_name = '../subscribers_modified.xml'
        userId = "520392515"
        parser = XMLParser(xml_file_name)
        start_date_list = parser.getStartDatesListByUser(userId)
        # print(start_date_list)

        self.assertEqual(2020,start_date_list[0].year)
        self.assertEqual(7, start_date_list[0].month)
        self.assertEqual(18, start_date_list[0].day)

        self.assertEqual(2020,start_date_list[1].year)
        self.assertEqual(6, start_date_list[1].month)
        self.assertEqual(18, start_date_list[1].day)

    def test_xml_parser_get_stop_date_list_when_there_are_2_duplications(self):
        xml_file_name = '../subscribers_modified.xml'
        userId = "520392515"
        parser = XMLParser(xml_file_name)
        stop_date_list = parser.getStopDatesListByUser(userId)
        # print(stop_date_list)

        self.assertEqual(2020,stop_date_list[0].year)
        self.assertEqual(10, stop_date_list[0].month)
        self.assertEqual(23, stop_date_list[0].day)

        self.assertEqual(2020,stop_date_list[1].year)
        self.assertEqual(6, stop_date_list[1].month)
        self.assertEqual(20, stop_date_list[1].day)

    def test_xml_parser_get_years_when_there_are_3_duplications(self):
        xml_file_name = '../subscribers_modified.xml'
        userId = "625998737"
        parser = XMLParser(xml_file_name)
        diff_list = parser.getDiffDateTimeListByUser(userId)
        # print([str(item) for item in diff_list])
        # print(parser.getDaysListByUser(userId))
        # print(97//7)
        # it's 364 days
        self.assertEqual(0, parser.getYearsListByUser(userId)[0])
        self.assertEqual(0, parser.getYearsListByUser(userId)[1])
        self.assertEqual(0, parser.getYearsListByUser(userId)[2])

    def test_xml_parser_get_months_when_there_are_3_duplications(self):
        xml_file_name = '../subscribers_modified.xml'
        userId = "625998737"
        parser = XMLParser(xml_file_name)
        diff_list = parser.getDiffDateTimeListByUser(userId)
        # print([str(item) for item in diff_list])
        # print(parser.getDaysListByUser(userId))
        # print(97//7)
        self.assertEqual(12,parser.getMonthsListByUser(userId)[0])
        self.assertEqual(0, parser.getMonthsListByUser(userId)[1])
        self.assertEqual(12, parser.getMonthsListByUser(userId)[2])

    def test_xml_parser_get_weeks_when_there_are_3_duplications(self):
        xml_file_name = '../subscribers_modified.xml'
        userId = "625998737"
        parser = XMLParser(xml_file_name)
        diff_list = parser.getDiffDateTimeListByUser(userId)
        # print([str(item) for item in diff_list])
        # print(parser.getDaysListByUser(userId))
        # print(97//7)
        self.assertEqual(52,parser.getWeeksListByUser(userId)[0])
        self.assertEqual(4, parser.getWeeksListByUser(userId)[1])
        self.assertEqual(52, parser.getWeeksListByUser(userId)[2])

    def test_xml_parser_get_days_when_there_are_3_duplications(self):
        xml_file_name = '../subscribers_modified.xml'
        userId = "625998737"
        parser = XMLParser(xml_file_name)
        # parser.showXMLforUser(userId)
        diff_list = parser.getDiffDateTimeListByUser(userId)
        # print([str(item) for item in diff_list])
        self.assertEqual(364,parser.getDaysListByUser(userId)[0])
        self.assertEqual(29, parser.getDaysListByUser(userId)[1])
        self.assertEqual(364, parser.getDaysListByUser(userId)[2])

if __name__ == '__main__':
    unittest.main()