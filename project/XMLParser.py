import xml.etree.ElementTree as ET
import json
from datetime import datetime, timedelta
from project.Abbo import Abbo

class XMLParser:

    def __init__(self, xml_file_name):
        self.xml_file_name = xml_file_name
        self.tree = ET.parse(xml_file_name)
        self.root = self.tree.getroot()

    def showXMLforUser(self, userId):
        for subscriber in self.root.findall("./subscriber/[user='" + userId + "']"):
            print(ET.tostring(subscriber, encoding='utf8').decode('utf8'))

    def showDatesByUser(self, userId):
        print("start, stop, diff")
        for subscriber in self.root.findall("./subscriber/[user='" + userId + "']"):
            startDate = subscriber.find("./start").text
            stopDate = subscriber.find("./stop").text
            print(startDate + ', ' + stopDate + ',')

    def getDiffDateTime(self, start_date, stop_date):
        start_datetime = datetime.strptime(start_date, '%Y%m%d')
        stop_datetime = datetime.strptime(stop_date, '%Y%m%d')
        return stop_datetime - start_datetime

    def getDiffDateTimeByUser(self, userId):
        for subscriber in self.root.findall("./subscriber/[user='" + userId + "']"):
            start_date_string = subscriber.find("./start").text
            stop_date_string = subscriber.find("./stop").text
        return self.getDiffDateTime(start_date_string, stop_date_string)

    def getDiffDateTimeFromList(self, dates_list):
        diff_list = []
        if len(dates_list) == 1:
            diff_list.append(self.getDiffDateTime(dates_list[0][0], dates_list[0][1]))
        else:
            for dates_item in dates_list:
                diff_list.append(self.getDiffDateTime(dates_item[0], dates_item[1]))
        return diff_list

    def getDiffDateTimeListByUser(self, userId):
        dates_list = []
        for subscriber in self.root.findall("./subscriber/[user='" + userId + "']"):
            start_date_string = subscriber.find("./start").text
            stop_date_string = subscriber.find("./stop").text
            dates_list.append([start_date_string,stop_date_string])
        return self.getDiffDateTimeFromList(dates_list)

    def getDaysByUser(self, userId):
        for subscriber in self.root.findall("./subscriber/[user='" + userId + "']"):
            start_date_string = subscriber.find("./start").text
            stop_date_string = subscriber.find("./stop").text
        start_datetime = datetime.strptime(start_date_string, '%Y%m%d')
        stop_datetime = datetime.strptime(stop_date_string, '%Y%m%d')
        diff = stop_datetime - start_datetime
        return diff.days

    def getDaysListByUser(self, userId):
        diff_dates_list = self.getDiffDateTimeListByUser(userId)
        return [item.days for item in diff_dates_list]

    def getStartDatesByUser(self, userId):
        for subscriber in self.root.findall("./subscriber/[user='" + userId + "']"):
            start_date_string = subscriber.find("./start").text
        start_datetime = datetime.strptime(start_date_string, '%Y%m%d')
        return start_datetime

    def getStartDatesListByUser(self, userId):
        start_date_list = []
        for subscriber in self.root.findall("./subscriber/[user='" + userId + "']"):
            start_date_string = subscriber.find("./start").text
            start_datetime = datetime.strptime(start_date_string, '%Y%m%d')
            start_date_list.append(start_datetime)
        return start_date_list

    def getStopDatesByUser(self, userId):
        for subscriber in self.root.findall("./subscriber/[user='" + userId + "']"):
            stop_date_string = subscriber.find("./stop").text
        stop_datetime = datetime.strptime(stop_date_string, '%Y%m%d')
        return stop_datetime

    def getStopDatesListByUser(self, userId):
        stop_date_list = []
        for subscriber in self.root.findall("./subscriber/[user='" + userId + "']"):
            stop_date_string = subscriber.find("./stop").text
            stop_datetime = datetime.strptime(stop_date_string, '%Y%m%d')
            stop_date_list.append(stop_datetime)
        return stop_date_list

    def getMonthsByUser(self, userId):
        return (self.getDaysByUser(userId) - (self.getDaysByUser(userId) % 30)) / 30

    def getMonthsListByUser(self, userId):
        return [(item // 30) for item in self.getDaysListByUser(userId)]

    def getWeeksByUser(self, userId):
        # TODO we need to use // probably
        return (self.getDaysByUser(userId) - (self.getDaysByUser(userId) % 7)) / 7

    def getWeeksListByUser(self, userId):
        return [(item // 7) for item in self.getDaysListByUser(userId)]

    def getYearsByUser(self, userId):
        return (self.getDaysByUser(userId) - (self.getDaysByUser(userId) % 364)) / 364

    def getYearsListByUser(self, userId):
        return [(item // 365) for item in self.getDaysListByUser(userId)]

    def getTimeDistribution(self, userId):

        years = int(self.getYearsByUser(userId))
        months = int(self.getMonthsByUser(userId))
        weeks = int(self.getWeeksByUser(userId))
        days = self.getDaysByUser(userId)

        return self.getTimeDistributionFromDates(years, months, weeks, days)

    def getTimeDistributionListByUser(self, userId):

        years_list = self.getYearsListByUser(userId)
        months_list = self.getMonthsListByUser(userId)
        weeks_list = self.getWeeksListByUser(userId)
        days_list = self.getDaysListByUser(userId)

        dates_dict = {}

        for i in range(0,len(years_list)):
            dates_dict["time_distribution_" + str(i)] = [years_list[i], months_list[i], weeks_list[i], days_list[i]]

        return [self.getTimeDistributionFromDateList(value) for value in dates_dict.values()]

    def getTimeDistributionFromDates(self, years, months, weeks, days):

        remaining_days = 0
        remaining_months = 0
        remaining_weeks = 0

        if (years > 0):
            remaining_months = months - 12 * years
            remaining_weeks = (days - 365 * years - remaining_months * 30) // 7
            remaining_days = days - 365 * years - remaining_weeks * 7 - 30 * remaining_months

        if (months > 0 and years == 0):
            remaining_months = months
            remaining_weeks = (days - 365 * years - months * 30) // 7
            remaining_days = days - 365 * years - remaining_weeks * 7 - 30 * remaining_months

        if (weeks > 0 and years == 0 and months == 0):
            remaining_weeks = weeks
            remaining_days = days - 365 * years - weeks * 7 - 30 * months

        if (days > 0 and weeks == 0 and years == 0 and months == 0):
            remaining_days = days

        time_distribution = dict()
        time_distribution["years"] = years
        time_distribution["months"] = months
        time_distribution["weeks"] = weeks
        time_distribution["days"] = days

        remaining_time_distribution = dict()

        remaining_time_distribution["years"] = years
        remaining_time_distribution["months"] = remaining_months
        remaining_time_distribution["weeks"] = remaining_weeks
        remaining_time_distribution["days"] = remaining_days

        return {"time_distribution": time_distribution, "remaining_time_distribution": remaining_time_distribution}

    def getTimeDistributionFromDateList(self, dates_list):

        years = dates_list[0]
        months = dates_list[1]
        weeks = dates_list[2]
        days = dates_list[3]

        return self.getTimeDistributionFromDates(years, months, weeks, days)

    def getAbboDistributionJSON(self, userId):
        time_distribution_list = self.getTimeDistributionListByUser(userId)
        start_date_list = self.getStartDatesListByUser(userId)

        abbo_list = []

        for i in range(0, len(start_date_list)):
            abbo_list.extend(self.getAbboList(start_date_list[i],time_distribution_list[i]))

        return json.dumps(abbo_list)

    def getAbboDistributionString(self, userId):

        time_distribution_list = self.getTimeDistributionListByUser(userId)
        start_date_list = self.getStartDatesListByUser(userId)
        abbo_list = []

        for i in range(0, len(start_date_list)):
            abbo_list.extend(self.getAbboList(start_date_list[i],time_distribution_list[i]))

        json_string = json.dumps([ob.__dict__ for ob in abbo_list])

        return json_string

    def getAbboList(self, start_datetime, td_dict):

        abbo_list = []
        updated_start_datetime = start_datetime
        next_datetime = start_datetime
        years = td_dict["remaining_time_distribution"]["years"]

        if (years > 0):
            for i in range(1, int(years) + 1):
                next_datetime = updated_start_datetime + timedelta(days=365)
                abbo_list.append(Abbo("abbo-365",
                                      datetime.strftime(updated_start_datetime, '%Y-%m-%d'),
                                      datetime.strftime(next_datetime, '%Y-%m-%d')))
                updated_start_datetime = next_datetime # + timedelta(days=1)

        months = td_dict["remaining_time_distribution"]["months"]

        if (months > 0):
            for i in range(1, int(months) + 1):
                next_datetime = updated_start_datetime + timedelta(days=30)
                abbo_list.append(Abbo("abbo-30",
                                      datetime.strftime(updated_start_datetime, '%Y-%m-%d'),
                                      datetime.strftime(next_datetime, '%Y-%m-%d')))
                updated_start_datetime = next_datetime # + timedelta(days=1)

        weeks = td_dict["remaining_time_distribution"]["weeks"]

        if (weeks > 0):
            for i in range(1, int(weeks) + 1):
                next_datetime = updated_start_datetime + timedelta(days=7)
                abbo_list.append(Abbo("abbo-7",
                                      datetime.strftime(updated_start_datetime, '%Y-%m-%d'),
                                      datetime.strftime(next_datetime, '%Y-%m-%d')))
                updated_start_datetime = next_datetime # + timedelta(days=1)

        days = td_dict["remaining_time_distribution"]["days"]

        if (days > 0):
            for i in range(1, int(days)+1):
                next_datetime = updated_start_datetime + timedelta(days=1)
                abbo_list.append(Abbo("abbo-1",
                                      datetime.strftime(updated_start_datetime, '%Y-%m-%d'),
                                      datetime.strftime(next_datetime, '%Y-%m-%d')))
                updated_start_datetime = next_datetime # + timedelta(days=1)

        return abbo_list
