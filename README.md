# ParserAPI

Simple REST API to query a subscribers DB.

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install ParserAPI.

```bash
pip install -r requirements.txt
```

## Usage

From the command line

```python
python app.py
```

If everything works file, you should get something like this message.

```bash
* Serving Flask app 'app' (lazy loading)
* Environment: production
   WARNING: This is a development server. Do not use it in a production deployment.
   Use a production WSGI server instead.
* Debug mode: on
* Running on all addresses.
   WARNING: This is a development server. Do not use it in a production deployment.
* Running on http://192.168.1.107:5000/ (Press CTRL+C to quit)
* Restarting with stat
* Debugger is active!
* Debugger PIN: 451-624-497
```
Copy and paste the reported URL. In order to run the query use the following endpoint

```python
http://<IP_ADDRESS>/findAbboByUser?userId=<USER_ID>
```
Where:

* USER_ID is the userId whose Abbos you are looking for 
* IP_ADDRESS is the IP address returned after running the application (in our case 192.168.1.107)

You can find the tests suite under the `tests` folder. It is possible to run unit test for `XMPParser` and functional tests for the REST API.


